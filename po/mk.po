# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Liljana Ackovska <liljanagjurova@gmail.com>, 2019
# Matej Plavevski <matej.plavevski+github@gmail.com>, 2019
# Zarko Gjurov <zarkogjurov@gmail.com>, 2019
msgid ""
msgstr ""
"Project-Id-Version: Tor Project\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-01-14 15:45+0100\n"
"PO-Revision-Date: 2019-10-08 22:35+0000\n"
"Last-Translator: Zarko Gjurov <zarkogjurov@gmail.com>\n"
"Language-Team: Macedonian (http://www.transifex.com/otf/torproject/language/"
"mk/)\n"
"Language: mk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : 1;\n"

#: ../onioncircuits:78
msgid "You are not connected to Tor yet..."
msgstr "Не сте поврзани на Tor се уште..."

#: ../onioncircuits:89
msgid "Onion Circuits"
msgstr "Onion Кругови"

#: ../onioncircuits:119
msgid "Close this circuit"
msgstr ""

#: ../onioncircuits:132
msgid "Circuit"
msgstr "Круг"

#: ../onioncircuits:133
msgid "Status"
msgstr "Статус"

#: ../onioncircuits:151
msgid "Click on a circuit for more detail about its Tor relays."
msgstr "Кликнете на кругот за повеќе детали за Tor релеа."

#: ../onioncircuits:261
msgid "The connection to Tor was lost..."
msgstr "Поврзувањето со Tor беше изгубено..."

#: ../onioncircuits:355
msgid ", "
msgstr ""

#: ../onioncircuits:357
msgid "..."
msgstr "..."

#: ../onioncircuits:387
#, c-format
msgid "%s: %s"
msgstr "%s:%s"

#: ../onioncircuits:617
msgid "GeoIP database unavailable. No country information will be displayed."
msgstr ""
"GeoIP базата е недостапна. Информациите за државите нема да бидат прикажани."

#: ../onioncircuits:647
#, c-format
msgid "%s (%s)"
msgstr "%s(%s)"

#: ../onioncircuits:651
#, c-format
msgid "%.2f Mb/s"
msgstr "%.2f Mb/s"

#: ../onioncircuits:653 ../onioncircuits:654
msgid "Unknown"
msgstr "Непознато"

#: ../onioncircuits:669
msgid "Fingerprint:"
msgstr "Отпечаток:"

#: ../onioncircuits:670
msgid "IP:"
msgstr "IP:"

#: ../onioncircuits:671
msgid "Bandwidth:"
msgstr "Проток:"

#~ msgid "Published:"
#~ msgstr "Објавено:"
