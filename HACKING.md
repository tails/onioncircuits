Coding style
============

Please follow PEP-8 "Style Guide for Python Code" [1] and be consistent with
current code when proposing patches. Explicit variables names are preferred,
even if they are longer.

[1] https://www.python.org/dev/peps/pep-0008/


Release
=======

1. Pull last commits:

        git checkout master
        git pull

2. Update translations:

        ${TAILS_GIT_REPO:?}/import-translations && \
        rm -f po/*.po~ && \
        git add po/*.po po/*.pot && \
        git commit po -m 'Update POT and PO files.'

3. Set `VERSION` to the desired version.

4. Apply the new version and verify it was applied:

        sed -i "s/version=\".*\",$/version=\"${VERSION:?}\",/" setup.py && \
        git diff --exit-code && echo "ERROR: new version was not applied"

4. Commit the version bump and push:

        git add setup.py && \
        git commit -m "Bump version to ${VERSION:?}" && \
        git push origin master

5. Create a merge request

6. Once the merge request has been merged, proceed with the next steps

7. Set `VERSION` to the desired version.

8. Tag the release and push:

        git tag -m "Release version $VERSION" -s "${VERSION:?}" && \
        git push --follow-tags origin master
